What is this?
=============

The code here is used to measure a voltage using an AtMega32 microcontroller
configured a bit like the Arduino Leonardo.  There are two channels of voltage
measurement.  As before, fixed potentiometers divide the external voltage (up
to 15volts) down to the maximum internal voltage of 2.56volts.  There is a
command language to drive the device as well as a rudimentary "calibration"
capability.

Later versions will use a digital potentiometer to divide the external voltage
down to something usable internally.  Calibration should still be allowed.
