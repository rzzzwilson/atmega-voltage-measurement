AtMega Voltage Measurement
==========================

Experiments on using the AtMega internal ADC and voltage references to measure
external voltages.  Eventually implement multiple channels, a control language
and a method of calibrating the device, if necessary.

All circuits will use the AtMega32U4 which gives us a serial port on the USB
connection to the controlling machine.

The testbed microcontroller is the Iota device, documented at
https://github.com/rzzzwilson/KiCad_Projects/tree/master/Iota .

The individual experiments, in increasing order of complexity, are:

* Simple        Very basic
* Dual          Two channels
* DualCommand   Two channels, control language
* DualAuto      Two channels, uses digital potentiometer
