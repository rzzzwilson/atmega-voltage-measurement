What is this?
=============

The code here is used to measure a voltage using an AtMega32 microcontroller
configured a bit like the Arduino Leonardo.  There are two channels of voltage
measurement.  A two-channel digital potentiometer is used as the divider thus
offering an "auto-range" capability.

As before, there is a control language as well as possibly a "calibrate"
feature.

STATUS: Waiting on the digital potentiometers to arrive from China!
