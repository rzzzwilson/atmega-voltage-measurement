/*
  AtMega32U4 voltage measurement test code.

  Measures two voltages, each with an "adjustment".

  Accepts commands:
    H;        print help text
    ID;       return string identifying the device
    NC;       get number of channels
    V?G;      get voltage for channel '?'
    C?G;      get calibration for channel '?'
    C?Sxxxx;  set calibration for channel '?'
    S?G;      get smoothing for channel '?'
    S?Sxx;    set smoothing for channel '?'
  where '?' is 0, 1, ..., choosing the channel to get/adjust.
 */

#include <EEPROM.h>


// Voltage measurement program name & version
const char *ProgramName = "Volts";
const char *Version = "1.1";

// number of channels in this version
const int NumChannels = 2;

// buffer, etc, to gather external command strings
#define MAX_COMMAND_LEN   16
#define COMMAND_END_CHAR    ';'
char CommandBuffer[MAX_COMMAND_LEN+1];
int CommandIndex = 0;

// pin assignments
int led = 12;           // D6 builtin led
int ChannelPins[NumChannels] = {23,   // F0 pin
                                22    // F1 pin
                               };

// value of internal reference
const float ARef = 2.56;

// plug in the measured divider values
float PotAdjust[NumChannels] = {(11.882 + 2.368) / 2.368,
                                (9.64 + 2.171) / 2.024
                               };

// max smoothing values for each channel
#define MaxSmoothing    10

// define stuff saved in EEPROM
const int CalibBaseAddress = 0;
const int SmoothingBaseAddress = CalibBaseAddress + NumChannels*sizeof(int);

// RAM copies of the data in EEPROM
int ChanCalib[NumChannels];
int ChanSmoothing[NumChannels];


void setup()
{
  // set pin modes, etc        
  analogReference(INTERNAL);   // 2.56 volt reference
  pinMode(led, OUTPUT);     
  Serial.begin(19200);
  delay(4000);                 // some delay in the system!?

  // copy EEPROM values into RAM arrays
  for (int chan = 0; chan < NumChannels; ++chan)
  {
    ChanCalib[chan] = get_eeprom_calibration(chan);
    ChanSmoothing[chan] = get_eeprom_smoothing(chan);
  }

  Serial.println("Ready.");
}

//----------------------------------------
// Get a smoothed voltage.
//----------------------------------------

float measure_volts(int chan)
{
  float volts_smooth = 0.0;
  int smooth = ChanSmoothing[chan];
  int pin = ChannelPins[chan];
  float adjust = PotAdjust[chan];
  float calib = ChanCalib[chan];

  for (int i = 0; i < smooth; ++i)
  {
    int raw_measure = analogRead(pin);
    volts_smooth += ARef * (raw_measure/1023.0) * (adjust + calib);
  }

  return volts_smooth / smooth;
}

//----------------------------------------
// Convert a string to uppercase in situ.
//----------------------------------------

void str2upper(char *str)
{
  while (*str)
  {
    *str = toupper(*str);
    ++str;
  }
}

//----------------------------------------
// Read/write the EEPROM.
//
// The supplied channel numbers are assumed correct and in-range.
//----------------------------------------

int get_eeprom_calibration(int chan)
{
  int result = 0;

  EEPROM.get(CalibBaseAddress + chan*sizeof(int), result);
  
  return result;
}

void put_eeprom_calibration(int chan, int calib)
{
  EEPROM.put(CalibBaseAddress + chan*sizeof(int), calib);
}

int get_eeprom_smoothing(int chan)
{
  int result = 0;

  EEPROM.get(SmoothingBaseAddress + chan*sizeof(int), result);
  
  return result;
}

void put_eeprom_smoothing(int chan, int smooth)
{
  EEPROM.put(SmoothingBaseAddress + chan*sizeof(int), smooth);
}

//##############################################################################
// External command routines.
//
// External commands are:
//     H;           send help text to console
//     ID;          get device identifier string
//     NC;          get number of channels
//     V?G;         get calibrated battery voltage for channel '?'
//     C?G;         get calibration for channel '?'
//     C?Sxxxx;     set calibration for channel '?'
//     S?G;         get smoothing for channel '?'
//     S?Sxxxx;     set smoothing for channel '?'
//##############################################################################

//----------------------------------------
// Get help:
//     H;
//----------------------------------------

const char * xcmd_help(char *answer, char *cmd)
{
  // to turn off warning about "unused parameter"
  cmd[0] = '\0';
  
  strcpy(answer, (char *) "\n-----------Interactive Commands-----------------\n");
  strcat(answer, (char *) "H;         send help text to console\n");
  strcat(answer, (char *) "ID;        get device identifier string\n");
  strcat(answer, (char *) "NC;        get number of channels\n");
  strcat(answer, (char *) "V?G;       get calibrated voltage from channel '?'\n");
  strcat(answer, (char *) "C?G;       get calibration for channel '?'\n");
  strcat(answer, (char *) "C?Sxxxx;   set calibration for channel '?'\n");
  strcat(answer, (char *) "S?G;       get smoothing for channel '?'\n");
  strcat(answer, (char *) "S?Sxx;     set smoothing for channel '?'\n");
  strcat(answer, (char *) "------------------------------------------------");
  return answer;
}

//----------------------------------------
// Get the identifier string:
//     ID;
//----------------------------------------

const char * xcmd_id(char *answer, char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "ID;"))
    return "ERROR";

  // generate ID string and return
  strcpy(answer, ProgramName);
  strcat(answer, " ");
  strcat(answer, Version);
  return answer;
}

//----------------------------------------
// Get the number of channels:
//     NC;
//----------------------------------------

const char * xcmd_nc(char *answer, char *cmd)
{
  // if not legal, complain
  if (strcmp(cmd, "NC;"))
    return "ERROR";

  // generate answer string and return
  sprintf(answer, "%d\n", NumChannels);
  return answer;
}

//----------------------------------------
// Get voltage of given channel:
//     V?G;
//----------------------------------------

const char * xcmd_vx(char *answer, char *cmd)
{
  // if not legal, complain
  if (strlen(cmd) != 4)
    return "ERROR";
  if (cmd[2] != 'G')
    return "ERROR";

  // get channel, check legal
  int channel = cmd[1] - '0';
  if ((channel < 0) or (channel >= NumChannels))
    return "ERROR";

  // get voltage for channel, adjusted for calibration
  float volts = measure_volts(channel);
  
  // generate answer string and return
  dtostrf(volts, 5, 2, answer);
  return answer;
}

//----------------------------------------
// Get/Set calibration for a channel:
//     C?G;
//     C?Sxxxx;
//----------------------------------------

const char * xcmd_cxy(char *answer, char *cmd)
{
  int calibration = 0;

  // if not legal, complain
  if (strlen(cmd) < 4)
    return "ERROR";

  // get channel number, check legal
  int channel = cmd[1] - '0';
  if ((channel < 0) or (channel >= NumChannels))
    return "ERROR";

  // do a GET
  if (cmd[2] == 'G')
  {
    if (cmd[3] != ';')
      return "ERROR";
    calibration = ChanCalib[channel];
    sprintf(answer, "%d\n", calibration);
    return answer;
  }
  // do a SET, change in-RAM as well as EEPROM
  else if (cmd[2] == 'S')
  {
    calibration = strtol(cmd + 3, NULL, 10);
    ChanCalib[channel] = calibration;
    put_eeprom_calibration(channel, calibration);
    return "OK";
  }
  
  return "ERROR";
}

//----------------------------------------
// Get/Set smoothing for a channel:
//     S?G;
//     S?Sxxxx;
//----------------------------------------

const char * xcmd_sxy(char *answer, char *cmd)
{
  int smoothing = 0;

  // if not legal, complain
  if (strlen(cmd) < 4)
    return "ERROR";

  // get channel number, check legal
  int channel = cmd[1] - '0';
  if ((channel < 0) or (channel >= NumChannels))
    return "ERROR";

  // do a GET
  if (cmd[2] == 'G')
  {
    if (cmd[3] != ';')
      return "ERROR";
    smoothing = ChanSmoothing[channel];
    sprintf(answer, "%d", smoothing);
    return answer;
  }
  // do a SET, change in-RAM as well as EEPROM
  else if (cmd[2] == 'S')
  {
    smoothing = strtol(cmd + 3, NULL, 10);
    if ((smoothing < 1) or (smoothing > MaxSmoothing))
      return "ERROR";
    ChanSmoothing[channel] = smoothing;
    put_eeprom_smoothing(channel, smoothing);
    return "OK";
  }
  
  return "ERROR";
}

//----------------------------------------
// Process an external command.
//     answer  address of place to store answer string
//     cmd     address of command string buffer
//     index   index of last char in string buffer
// 'cmd' is '\0' terminated.
// Returns the command response string.
//----------------------------------------
const char * do_external_cmd(char *answer, char *cmd, int index)
{
  char end_char = cmd[index];

  // ensure everything is uppercase
  str2upper(cmd);

  // if command too long it's illegal
  if (end_char != COMMAND_END_CHAR)
  {
    return (char *) "TOO LONG";
  }

  // process the command
  switch (cmd[0])
  {
    case 'C':
      return xcmd_cxy(answer, cmd);
    case 'H':
      return xcmd_help(answer, cmd);
    case 'I':
      return xcmd_id(answer, cmd);
    case 'N':
      return xcmd_nc(answer, cmd);
    case 'S':
      return xcmd_sxy(answer, cmd);
    case 'V':
      return xcmd_vx(answer, cmd);
  }

  return xcmd_help(answer, cmd);
}

void do_external_commands(void)
{
  // gather any commands from the external controller
  while (Serial.available()) 
  {
    char ch = Serial.read();

    // ignore newlines
    if (ch == '\n')
      continue;
    
    if (CommandIndex < MAX_COMMAND_LEN)
    { 
      // only add if command not over-length
      CommandBuffer[CommandIndex++] = ch;
    }
    
    if (ch == COMMAND_END_CHAR)   // if end of command, execute it
    {
      char answer[512];           // place to construct answer strings
      
      CommandBuffer[CommandIndex] = '\0';
      sprintf(answer, "%s\n", do_external_cmd(answer, CommandBuffer, CommandIndex-1));
      Serial.print(answer);
      CommandIndex = 0;
    }
  }
}

void loop()
{    
  // do any external commands
  do_external_commands();
  delay(100);   // not too fast
}
