/*
  AtMega32U4 voltage measurement test code.

  Runs on the iota Teensy 2 clone.  Like a Leonardo with different pins.

  Next: implement a command/response solution, with adjustment commands.
 */

#include <string.h>
#include <LiquidCrystal.h>

// pin allocations
int led = 12;           // D6 builtin led
int Vin = 23;           // F0 pin

// value of internal reference
const float ARef = 2.56;

// plug in the measured divider values
float Adjustment = -0.016;  // this should be adjustable via serial port
const float PotAdjust = (11.882 + 2.368) / 2.368 + Adjustment;

void setup()
{           
  analogReference(INTERNAL);   // 2.56 volt reference
  pinMode(led, OUTPUT);     
  Serial.begin(19200);
  Serial.println("Ready");
}

void loop()
{    
  static long loop_count = 0;
  float result;

  long delta = micros();
  result += ARef * (analogRead(Vin)/1023.0) * PotAdjust;
  delta = micros() - delta;
  
  Serial.print(loop_count);
  Serial.print(": ");
  Serial.print(result); 
  Serial.print(", took ");
  Serial.print(delta);
  Serial.println("us");

  loop_count += 1;

  delay(1000);
}
