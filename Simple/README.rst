What is this?
=============

The code here is used to measure a voltage using an AtMega32 microcontroller
configured a bit like the Arduino Leonardo.  A potentiometer is used to divide
an external voltage up to 15volts down to the maximum measurable voltage, set
at 2.56volts since we are using the 2.56v internal reference.

The code is very basic and has hard-coded divider values that are "tweaked" by
hand to get an accurate value.  Later versions will offer two voltage channels,
a command language to control the device from a desktop machine, and the ability
to calibrate the measurements.
