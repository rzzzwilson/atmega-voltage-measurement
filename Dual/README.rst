What is this?
=============

The code here is used to measure a voltage using an AtMega32 microcontroller
configured a bit like the Arduino Leonardo.  There are two channels of voltage
measurement.  As before, fixed potentiometers divide the external voltage (up
to 15volts) down to the maximum internal voltage of 2.56volts.

The code is very basic and has hard-coded divider values that are "tweaked" by
hand to get an accurate value.  Later versions will offer a command language to
control the device from a desktop machine, and the ability to calibrate the
measurements.
