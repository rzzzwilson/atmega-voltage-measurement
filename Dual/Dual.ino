/*
  AtMega32U4 voltage measurement test code.

  Runs on the iota Teensy 2 clone.  Like a Leonardo with different pins.

  Measures two voltages, each with an "adjustment".

  Next: implement a command/response solution, with adjustment commands.
 */

#include <string.h>
#include <LiquidCrystal.h>

// pin allocations
int led = 12;           // D6 builtin led
int Vin1 = 23;          // F0 pin
int Vin2 = 22;          // F1 pin

// value of internal reference
const float ARef = 2.56;

// plug in the measured divider values
float Adjustment1 = -0.016;  // this should be adjustable via serial port
const float Pot1Adjust = (11.882 + 2.368) / 2.368 + Adjustment1;
float Adjustment2 = 0.67;  // this should be adjustable via serial port
const float Pot2Adjust = (9.64 + 2.171) / 2.024 + Adjustment2;

void setup()
{           
  analogReference(INTERNAL);   // 2.56 volt reference
  pinMode(led, OUTPUT);     
  Serial.begin(19200);
  Serial.println("Ready");
}

void loop()
{    
  static long loop_count = 0;
  float result1;
  float result2;

  result1 += ARef * (analogRead(Vin1)/1023.0) * Pot1Adjust;
  result2 += ARef * (analogRead(Vin2)/1023.0) * Pot2Adjust;
  
  Serial.print(loop_count);
  Serial.print(": ");
  Serial.print(result1); 
  Serial.print(", ");
  Serial.println(result2);

  loop_count += 1;

  delay(1000);
}
